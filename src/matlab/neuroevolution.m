%% Initialization
clear; close all; clc

addpath('./lib');
addpath('./lib/coursera');

drawplots = 0;
showimages = 0;

h = 2; % hidden units

% ORL dataset
% [X,y] = orl_dataset_2dpca;

% Iris dataset
[X,y] = iris_dataset; X=X';y=y';

% Handwritten digits
% [D,y] = digittrain_dataset; D=D';y=y';
% y = y(1:100,:);
% X = zeros(100, size(D{1},1)^2);
% for i=1:size(X,1)
%    X(i,:) = reshape(D{i},1,size(D{i},1)^2);
% end

% Xor dataset
% load('xor.mat');
% X = xor(:, 1:2);
% y = xor(:, 3:end);
% X = logical(randi([0,1],100,2));
% y = zeros(size(X,1),2);
% y(:,1) = xor(X(:,1),X(:,2));
% y(:,2) =  ~(xor(X(:,1),X(:,2)));

% randomization of data
% data = [X y];
% perm = randperm(size(data,1));
% data = data(perm,:);
% X = data(:,1:k);
% y = data(:, k+1:end);

% get dimensions of data
k = size(X,2);
m = size(X,1);
n = size(y,2);

% Show images (in case of ORL dataset)
if (showimages == 1)
    D = reshape(getORLdata(255),m,height*width);
    % sel = randperm(size(D, 1));
    % sel = sel(1:100);
    displayData(D(1:400, :), width);
    fprintf('Program paused. Press enter to continue.\n');
    pause;
end

nVar = (k+1)*h + (h+1)*n + 1; % number of variables (+biases)
lb = zeros(nVar,1); % lower boundary
ub = ones(nVar,1); % upper boundary
IntCon = 1:nVar;

costFunction = @(p) buildCourseraNN(p, X, y, h);
options = gaoptimset('PlotFcns', {@gaplotbestf, @gaplotstopping});
options = gaoptimset(options, 'CreationFcn', @gacreationuniform);
options = gaoptimset(options, 'Generations', 10);
options = gaoptimset(options, 'PopulationSize', 10);
% options = gaoptimset(options, 'Vectorize', 'on');
[x,fval] = ga(costFunction, nVar, [], [], [], [], lb, ub, [], [], options);
