%% Initialization
% clear; close all; clc

addpath('./lib');
addpath('./lib/coursera');
addpath('./sigmoidApprox');

% Dataset
[X,y] = orl_dataset_2dpca;
X = mapminmax(X);
% load('weights_pruned.mat');
% weights = weights_export;

% load('weights_transposed.mat');
weights = weights_export;


% constants
BIT_WIDTH = 8;
FRACTION = 4;

% options
imageIndex = 5;
neuronLogFile = fopen('reference/neuron.log', 'w');
% logFileBin = fopen('debugBin.log', 'w');
neuronLevelLogFileBin = fopen('reference/neuronLogBin.csv', 'w');
logFileBin = fopen('reference/debug.csv', 'w');
inputFile = fopen('reference/input.log', 'w');
weightFile = fopen('reference/weight.log', 'w');
sumFile = fopen('reference/sum.log', 'w');
outputFile = fopen('reference/output.log', 'w');

% Neural network dimensions
layers = 2;
[~, k] = size(X);
n = size(y,2);
h = [40, n];

% Split data into training, testing and validation datasets
data_ratios = [0.8, 0.2, 0];
[trainX, testX, ~] = splitDataORL(X,40,data_ratios);
[trainy, testy, ~] = splitDataORL(y,40,data_ratios);

% Transpose weights
Theta1 = reshape(weights(1:h(1) * (k + 1)), h(1), (k + 1));
Theta2 = reshape(weights((1 + (h(1) * (k + 1))):end), n, (h(1) + 1));
Theta1_t = Theta1';
Theta2_t = Theta2';
newweights = [Theta1_t(:); Theta2_t(:)];

% calculate result
F = fimath('SumMode', 'SpecifyPrecision', ...
            'SumWordLength', 2*BIT_WIDTH, ...
            'SumFractionLength', 2*FRACTION, ...
            'RoundingMethod', 'Floor', ...
            'OverflowAction', 'Saturate');
% sum = 0;
sum = fi(0, 1, 2*BIT_WIDTH, 2*FRACTION);
sum = setfimath(sum, F);
maxn = 0;
maxi = 0;
index = imageIndex;
m = [size(X, 2), h(1)];
w_counter = 1;
input = [ones(size(testX,1), 1), testX];
outputs = zeros(h(1), 1);
% go through layers
for l = 1:layers
    % go through neurons
    for i = 1:h(l)
        % go through inputs
        for j=1:m(l)+1
            % input and weight to fixed point
            fiInput = fi(input(index,j),1,BIT_WIDTH, FRACTION);
            fiWeight = fi(newweights(w_counter, 1), 1, BIT_WIDTH, FRACTION);

            % calculating
            sum = sum + fiInput * fiWeight;
            truncsum = fi(sum, 1, BIT_WIDTH, FRACTION);
            output = fi(1/(1+exp(-double(truncsum))), 1, BIT_WIDTH, FRACTION);
%             output = fi(sigmoidCRIInitial(double(truncsum)), 1, BIT_WIDTH, FRACTION);
            outputs(i) = output;

            % convert to binary
            binInput = fiInput.bin;
            binWeights = fiWeight.bin;
            binSum = truncsum.bin;
            binOutput = output.bin;

            % save as comma separated file
            fprintf(logFileBin, '%d,%d,%d,%s,%s,%s,%s\n', l, i, j, binInput, binWeights, binSum, binOutput);
            % save to separate files
            fprintf(inputFile, '%s\n', binInput);
            fprintf(weightFile, '%s\n', binWeights);
            fprintf(sumFile, '%s\n', binSum);
            fprintf(outputFile, '%s\n', binOutput);
            w_counter = w_counter + 1;
        end
        % write all the neuron final outputs  to file
        fprintf(neuronLogFile, '%s\n', binOutput);
        % fprintf(neuronLogFile, '%d,%d,%f,%f\n', l, i, sum, output);
        fprintf('%d,%d\n', l, i); % print to console
        if (l == layers) % when last layer
            if (output > maxn)
                maxn = output;
                maxi = i;
            end
        end
        sum = 0;
    end
    input = [1 outputs'];
    index = 1;
end

fprintf('Classified as %d\n', maxi);
[a,b] = max(testy, [], 2);
fprintf('Actually %d\n', b(imageIndex));
close('all');
