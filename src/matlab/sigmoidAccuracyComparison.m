%% Initialization
clear; close all; clc
addpath('./lib');
addpath('./lib/coursera');
addpath('./sigmoidApprox');

% Dataset 
[X,y] = orl_dataset_2dpca;
X = mapminmax(X);

% dimensions 
h = 40;
[~, k] = size(X);
n = size(y,2);

% getting the weights
load('weights.mat');
Theta1 = reshape(weights(1:h * (k + 1)), h, (k + 1));
Theta2 = reshape(weights((1 + (h * (k + 1))):end), n, (h + 1));

% options
maxBitWidth = 8;
maxRange = 8;

% Load from file
loadFromFile = 0;
if loadFromFile(1)
    load('sigmoidaccuracies.mat');
else
acc = zeros(1,maxBitWidth);
%% Trying different accuracies
for i=1:maxBitWidth
    for j = 1:maxRange
        output = predictLUT(Theta1, Theta2, X, i, j);
        acc(i,j) = mean(all(compet(output')' == y,2)) * 100;

        % Log
        fprintf('Bit_width: %d | Range: %d | Accuracy: %.2f\n', i, j, acc(i,j));
    end
    fprintf('-------------------------------------\n');
end
end
%% Plotting
% for i=1:maxBitWidth
    error = 100-acc;
    bar3(1:maxRange, acc);
    hold on;
    title('Sigmoid LUT approximation accuracy');
    ylabel('Bit width');
    xlabel('Range');
    zlabel('Accuracy of the network in %');
% end
legend(gca, 'show');