%% Initialization
clear; close all; clc

addpath('./lib');
addpath('./lib/coursera');
addpath('./sigmoidApprox');

% Dataset 
[X,y] = orl_dataset_2dpca;

X = mapminmax(X);

% options
displayData = 0;
drawplots = 1;
printLog = 1;
epochs = 30;
gradient_iterations = 10;
options = optimset('MaxIter', gradient_iterations);
experiments = 1;
    
% Lambda - regularization parameter. For controlling overfittinpg
lambda = 0.4; % can be added to gene later on
    
% Neural network dimensions
h = 40;
[~, k] = size(X);
n = size(y,2);

%% Training neural network 
% experimentAcc=zeros(experiments,1);
% for lambda=0:0.1:2
for e=1:experiments
    % random weight initialization
    initial_Theta1 = randInitializeWeights(k, h);
    initial_Theta2 = randInitializeWeights(h, n);

    % Unroll parameters
    weights = [initial_Theta1(:) ; initial_Theta2(:)];

    % randomize
    %     data = [X y];
    %     perm = randperm(size(data,1));
    %     data = data(perm,:);
    %     X = data(:,1:k);
    %     y = data(:, k+1:end);

    % Split data into training, testing and validation datasets
    data_ratios = [0.8, 0.2, 0];  
    % [trainX, testX, ~] = split(X,data_ratios);
    % [trainy, testy, ~] = split(y,data_ratios);
    [trainX, testX, ~] = splitDataORL(X,40,data_ratios);
    [trainy, testy, ~] = splitDataORL(y,40,data_ratios);
    
    % randomize
    perm = randperm(size(trainX,1));
    trainX = trainX(perm,:);
    trainy = trainy(perm,:);

    perm = randperm(size(testX,1));
    testX = testX(perm,:);
    testy = testy(perm,:);
    

    trainAcc = zeros(epochs,1);
    testAcc = zeros(epochs,1);
    LUTAcc = zeros(epochs,1);
    CRIAcc = zeros(epochs,1);

    costFunction = @(p) nnCostFunction(p, k, h, n, trainX, trainy, lambda);
    tic % measure training time
    for i = 1:epochs
        % Train
        [nn_params, ~] = fmincg(costFunction, weights, options);
        
        % Obtain Theta1 and Theta2 back from nn_params
        Theta1 = reshape(nn_params(1:h * (k + 1)), h, (k + 1));
        Theta2 = reshape(nn_params((1 + (h * (k + 1))):end), n, (h + 1));

        %% Evaluate
        % Get the predictions of trained network
        trainPred0 = predict(Theta1, Theta2, trainX);
        testPred0 = predict(Theta1, Theta2, testX);
        testLUT0 = predictLUT(Theta1, Theta2, testX,4,6);
        testCRI0 = predictCRI(Theta1, Theta2, testX);
        % Find the class with highest value
        trainPred = compet(trainPred0')';
        testPred = compet(testPred0')';
        LUTPred = compet(testLUT0')';
        CRIPred = compet(testCRI0')';
        % Get accuracy
        trainAcc(i) = mean(all(trainPred == trainy,2)) * 100;
        testAcc(i) = mean(all(testPred == testy,2)) * 100;
        LUTAcc(i) = mean(all(LUTPred == testy,2)) * 100;
        CRIAcc(i) = mean(all(CRIPred == testy,2)) * 100;
        weights = [Theta1(:) ; Theta2(:)];

        %% Plotting   
        if drawplots == 1
            % Find if figure already open
            fig1 = findobj('type','figure','name','fig1');
            if isempty(fig1)
                figure('name', 'fig1');
            else
                clf(fig1); % clear the figure
                set(0, 'CurrentFigure', fig1); % select the figure
            end

            % error plot
            hold all;
            trainError = 100-trainAcc;
            testError = 100-testAcc;
            LUTError = 100-LUTAcc;
            CRIError = 100-CRIAcc;
%             plot(1:i, trainError(1:i), 'black');
            plot(1:i, testError(1:i), 'black');
            plot(1:i, LUTError(1:i), 'black--');
            plot(1:i, CRIError(1:i), 'black-.');
            xlabel('Epochs');
            ylabel('Error %');
            legend('Test', 'LUT', 'CRI');
            
            indexmin = find(min(testError) == testError);
            ymin = testError(indexmin(end));
            strmin = [num2str(ymin)];
            text(indexmin(end),ymin,[strmin ' %'],'HorizontalAlignment','left', 'VerticalAlignment', 'top');
            
            axis([1,epochs, 0, 100]);
            drawnow;
        end
    end
    trainingTime = toc;

    experimentAcc(e) =  max(testAcc);
end

%% display pics
if (displayData == 1) 
    rawX = orl_dataset_raw;
    [~, rawTestX, ~] = splitData(rawX,40,data_ratios);
    rawTestX = rawTestX(perm,:);
    [~, indy] = max(testy, [], 2);
    [~, indp] = max(testPred,[],2);
    displayDataWithTargets(rawTestX,92,indy,indp);
end

%% Log
if printLog == 1
    fprintf('Testing with lambda %.2f\n', lambda);
    fprintf('Average test accuracy: %.2f\n', mean(experimentAcc));
    fprintf('------------------\n');
end
% end