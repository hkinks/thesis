x = -5:0.1:5;
linear = x;

binary = zeros(1,size(x,2));
for i = 1:size(x,2);
   if (x(i) >= 0)
       binary(i) = 1;
   end
end

sigmoid = 1./(1+exp(-x));
tansig = 2./(1+exp(-2.*x))-1;
subplot(2,2,1);

plot(x,linear, 'black');
title('Linear function');
grid on;
grid minor;

subplot(2,2,2);
plot(x,binary, 'black');
title('Step function');
grid on;
grid minor;

subplot(2,2,3);
plot(x,sigmoid, 'black');
title('Sigmoid function');
grid on;
grid minor;

subplot(2,2,4);
plot(x,tansig, 'black');
title('TanSig function');
grid on;
grid minor;