function [ cost, nn_params ] = buildCourseraNN( p, X , y, hidden_neurons )
    % options
    drawplots = 1;
    printLog = 1;
    epochs = 10;
    gradient_iterations = 10;
    options = optimset('MaxIter', gradient_iterations);

    % Lambda - regularization parameter. For controlling overfittinpg
    lambda = 0.3; % can be added to gene later on

    h = hidden_neurons;
    [~, k] = size(X);
    n = size(y,2);

    % save gene to workspace
    assignin('base', 'gene', p);

    % Unrolling the gene
    inputGene = reshape(p(1,1:h*(k+1)), h, k+1);
    hiddenGene = reshape(p(1,h*(k+1)+1:end-1), n, h+1);

    % get the probability of forming connections
    prob = p(end);

    % form connections according to probability
    inputConnections = (inputGene < prob);
    hiddenConnections = (hiddenGene < prob);

    % random weight initialization
    initial_Theta1 = randInitializeWeights(k, h);
    initial_Theta2 = randInitializeWeights(h, n);

    % Make the unconnected synapes zero
    initial_Theta1 = initial_Theta1 .* inputConnections;
    initial_Theta2 = initial_Theta2 .* hiddenConnections;

    % Unroll parameters
    weights = [initial_Theta1(:) ; initial_Theta2(:)];

    % randomize
%     data = [X y];
%     perm = randperm(size(data,1));
%     data = data(perm,:);
%     X = data(:,1:k);
%     y = data(:, k+1:end);

%       split data into training, testing and validation datasets
    data_ratios = [0.8, 0.2, 0];
    [trainX, testX, ~] = split(X,data_ratios);
    [trainy, testy, ~] = split(y,data_ratios);
%     [trainX, testX, ~] = splitData(X,40,data_ratios);
%     [trainy, testy, ~] = splitData(y,40,data_ratios);

    trainAcc = zeros(epochs,1);
    testAcc = zeros(epochs,1);

    costFunction = @(p) nnCostFunction(p, k, h, n, X, y, lambda);
    tic
    for i = 1:epochs
        % Now, costFunction is a function that takes in only one argument (the
        % neural network parameters)
        [nn_params, ~] = fmincg(costFunction, weights, options);

        % Obtain Theta1 and Theta2 back from nn_params
        Theta1 = reshape(nn_params(1:h * (k + 1)), h, (k + 1));
        Theta2 = reshape(nn_params((1 + (h * (k + 1))):end), n, (h + 1));

        trainPred = predict(Theta1, Theta2, trainX);
        testPred = predict(Theta1, Theta2, testX);
        trainPred = compet(trainPred')';
        testPred = compet(testPred')';
        trainAcc(i) = mean(all(trainPred == trainy,2)) * 100;
        testAcc(i) = mean(all(testPred == testy,2)) * 100;
%         [~, y] = max(y, [], 2);
%         [~, testy] = max(testy, [], 2);
%         trainAcc(i) = mean(double(trainPred == y)) * 100;
%         testAcc(i) = mean(double(testPred == testy)) * 100;
        weights = [Theta1(:) ; Theta2(:)];
    end

    % plotting
    % plot the network under test
    if drawplots == 1 %&& rand()<0.05
        fig1 = findobj('type','figure','name','fig1');
        if isempty(fig1)
            figure('name', 'fig1');
        else
            clf(fig1);
            set(0, 'CurrentFigure', fig1);
        end
%         set(fig1, 'WindowStyle', 'docked');
%         figure(fig1); % make figure selected

        %%% Digraph available from Matlab 2016 ! %%%
        subplot(2,1,1);
        firstCol = zeros(1, sum(sum(inputConnections)));
        secondCol = zeros(1, sum(sum(inputConnections)));
        index = 1;
        for i=1:size(inputConnections,2)
            for j=1:size(inputConnections,1)
                if inputConnections(j,i) == 1
                    firstCol(index) = i;
                    secondCol(index) = size(inputConnections,2) + j;
                    index = index + 1;
                end
            end
        end
        G = digraph(firstCol, secondCol);
%         plot(G, 'Layout', 'layered', 'Sources', 1:225, 'Sinks', 226:265);
        plot(G, 'Layout', 'layered');

        % error plot
        subplot(2,1,2);
        hold all;
        plot(1:size(trainAcc), 100-trainAcc, 'b');
        plot(1:size(testAcc), 100-testAcc, 'g');
        axis([0,epochs, 0, 100]);
        drawnow;
    end


    % find number of zero connections
    zerosN = sum(sum(Theta1==0)) + sum(sum(Theta2==0));
    totalConnections = size(Theta1,1)*size(Theta1,2)+size(Theta2,1)*size(Theta2,2);

%     errorPercentage = 100-max(testAcc);
%     errors = 100-testAcc;
%     mse = mean(errors.^2);
    minTrainError = 100-max(trainAcc);
    minError = 100-max(testAcc);
    trainingTime = toc;
    connectionsPercentage = (1-(zerosN/totalConnections))*100;
    cost = connectionsPercentage + minTrainError^2 + minError^2 + trainingTime;

    % log
    if printLog == 1
        fprintf('Training time %.2f\n', trainingTime);
        fprintf('Testing with lambda %.2f\n', lambda);
        fprintf('Number of zero connections %d (%.2f%% connected)\n', zerosN, connectionsPercentage);
        fprintf('Train acc: %f \tTest acc: %f\n', max(trainAcc), max(testAcc));
%         fprintf('Gradient cost: %.2f\n', grad_cost);
        fprintf('Cost: %.2f\n', cost);
        fprintf('------------------\n');
    else
        fprintf('.');
    end

    % save cost to workspace
    try
        bestCost = evalin('base', 'bestCost');
        if cost < bestCost
            assignin('base', 'bestCost', cost);
            assignin('base', 'bestGene', p);
            assignin('base', 'bestInputConnections', inputConnections);
            assignin('base', 'bestHiddenConnections', hiddenConnections);
            bestWeights = [Theta1(:) ; Theta2(:)];
            assignin('base', 'bestWeights', bestWeights);
        end
    catch
        assignin('base', 'bestCost', cost);
    end

end
