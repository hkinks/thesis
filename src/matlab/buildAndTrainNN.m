function [ cost, net ] = buildAndTrainNN( p, X , y, maxLayers )
% tic;
fprintf('Gene: ');
fprintf('%d ', p);
fprintf('\n');

transferFnList = {'tansig', 'logsig', 'purelin', 'softmax'};
data_ratios = [0.8, 0.2, 0]; % let's divide data for custom validation 
[trainX, validX, ~] = splitDataORL(X,40,data_ratios);
[trainy, validy, ~] = splitDataORL(y,40,data_ratios);

try
    numLayers = p(1);
    numInputs = 1;
%     numOutputs = size(y, 2);
    neurons = p(2:numLayers+1);
    biasConnect = reshape(p(maxLayers+2:maxLayers+numLayers+1), numLayers, 1);
%     biasConnect = ones(numLayers,1);
%     size(biasConnect)
    transferFns = p(maxLayers*2+2:maxLayers*2+numLayers+1);
    layerConnect = reshape(p(maxLayers*3+2: maxLayers*3+numLayers*numLayers+1), numLayers, numLayers)';
    inputConnect = reshape(p(maxLayers*3+maxLayers^2+2: ...
       maxLayers*3+maxLayers^2+numLayers*numInputs+1), numLayers, numInputs);
%     outputConnect = reshape(p(maxLayers*3+maxLayers^2+maxLayers*numInputs+2: ...
%         maxLayers*3+maxLayers^2+maxLayers*numInputs+numLayers+1), 1, numLayers);
    outputConnect = zeros(1,numLayers);
    outputConnect(numLayers) = 1;

    inputConnect(1,1)=1; % make sure that something is connected to input

    if numLayers > 1 
        net = patternnet(neurons(1:numLayers-1));
    else
        net = patternnet([]);
    end
%     net = network(numInputs, numLayers, biasConnect, inputConnect, layerConnect, outputConnect);

    net.biasConnect = biasConnect;
    net.inputConnect = inputConnect;
    net.outputConnect = outputConnect;
    net.layerConnect = layerConnect;
    
    if ( p(maxLayers*3+maxLayers^2+2) == 1) 
        net.inputs{1}.processFcns = {'mapminmax'};
        net.outputs{2}.processFcns = {'mapminmax'};
    end
    for i = 1:numLayers
       net.layers{i}.transferFcn = char(transferFnList(transferFns(i)));
       net.layers{i}.size = neurons(i);
       net.layers{i}.initFcn = 'initnw';
       net.layers{i}.name = strcat('layer #', int2str(i));

       net.biases{i}.learnFcn = 'learngdm';
       net.inputWeights{i}.learnFcn = 'learngdm';
       
       % set connection delays
       for j = 1:numLayers
           if net.layerConnect(i,j) == 1 && j>=i % backward connection
               net.layerWeights{i,j}.delays = ones(1);
           end
       end
    end
%     net.layers{numLayers}.transferFcn = 'softmax';
    net.layers{numLayers}.size = 40;
    
    % layers = p(2:numLayers+1);
%     net = patternnet(40);
    net.divideFcn = 'dividerand';
    net.divideParam.trainRatio = 1;
    net.divideParam.valRatio = 0;
    net.divideParam.testRatio = 0;
    net.name = strcat('Test subject number #', int2str(rand*1000));
%     net.performFcn = 'mse';
    net.trainFcn = 'trainscg';
     net.adaptFcn = 'adaptwb';
    net.performFcn = 'crossentropy';
    net.plotFcns = {'plotperform', 'plottrainstate', 'ploterrhist', 'plotconfusion', 'plotroc'};
    net.trainParam.epochs = 100; 
%     net.trainParam.min_grad = 1e-10;
    net.trainParam.time = 10; % Maximum training time in seconds
    net.trainParam.showWindow = 1;
    net = configure(net, trainX', trainy'); 
    net = init(net);
%     view(net);
    tic
    [net, training] = train(net,trainX',trainy','useGPU','yes');
    trainingTime = toc;
    % pred = vec2ind(round(net(X')));
%     cost = training.best_perf;
%     fprintf('%d layers: cost %f\n', numLayers, cost);
    output = net(validX');
    [errors, ~] = confusion(validy', output);
    acc = 100*(1-errors);
    cost = errors * 100 + training.best_perf + trainingTime;
    fprintf('\tAcc: %.2f%%\n', acc);
catch
    cost = 1000;
end
    fprintf('\tCost: %.2f\n', cost);
end