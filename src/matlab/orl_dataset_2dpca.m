function [ inputs, targets ] = orl_dataset_2dpca
%ORL_DATASET Loads ORL faces dataset
height = 112;
dim = 2;
k = dim*height; % number of inputs / resolution of image
m = 400; % total number of examples

n = 40; % number of classes / number of subjects
inputs = reshape(PCA_2D(getORLdata(255,n),dim), m, k); % pca2d
targets = calculateLabels(m,n,1);
end

