close all;
figure;

wordSize = 8;
fractionSize = 4;
range = 16;
step = 0.1;
input = -range:step:range;
lutRange = 5;
lutWidth = 4; 

%% sigmoid
output_sigmoid = sigmoid(input);

%% sigmoid approximation CRI initial
q = 0;
output_cri1 = zeros(1,size(input,2));
for i=1:size(input,2)
    output_cri1(i) = sigmoidCRI(input(i), q);
end

%% sigmoid approximation CRI 1 iteration
q = 1;
output_cri2 = zeros(1,size(input,2));
for i=1:size(input,2)
    output_cri2(i) = sigmoidCRI(input(i), q);
end

%% sigmoid approximation LUT
% lut_width = 4;
% range = 2;
% stepsize = range*2/2^lut_width; 
% input_d = -range:stepsize:range;
% input_fi = fi(input_d, 1, word_size, fraction_size);
% output_d = sigmoid(input_d);
% output_fi = fi(output_d, 1, word_size, fraction_size);
% range = 2;
output_lut = zeros(1, size(input,2));
for i = 1:size(input,2)
    output_lut(i) = sigmoidLUT(input(i), lutWidth, lutRange);
end

output_fi = fi(output_lut, 1, wordSize, fractionSize);
input_fi = fi(input, 1, wordSize, fractionSize);

%% Matlab LUT approx
func ='sigmoid(x)';
% func = 'sin(2*pi*x)';
% Define the range over which to optimize breakpoints
xmin = -lutRange;
xmax = lutRange;
% Define the data type and scaling for the inputs
xdt = sfix(wordSize);
xscale = 2^-lutWidth;
% Define the data type and scaling for the outputs
ydt = sfix(wordSize);
yscale = 2^-lutWidth;
% Specify the rounding method
rndmeth = 'Floor';
% Define the maximum acceptable error
errmax = 2^-10;
% Choose even, power-of-2 spacing for breakpoints
spacing = 'pow2';
% Create the lookup table
[xdata,ydata,errworst] = fixpt_look1_func_approx(func,...
 xmin,xmax,xdt,xscale,ydt,yscale,rndmeth,errmax,[],spacing);
fixpt_look1_func_plot(xdata,ydata,func,xmin,xmax,...
 xdt,xscale,ydt,yscale,rndmeth);

xdata_bin = fi(xdata, 1, wordSize, fractionSize);
ydata_bin = fi(ydata, 1, wordSize, fractionSize);

xdata_bin = xdata_bin.bin;
ydata_bin = ydata_bin.bin;
%% Plot
% subplot(1,2,1);
% hold on;
% plot(input,output_sigmoid, 'black');
% plot(input, output_cri1, 'black--');
% xlabel('z');
% ylabel('H(z)');
% legend('Sigmoid function', 'CRI (iterations=0)', 'LUT', 'Location','southeast');
% 
% subplot(1,2,2);
% hold on;
% plot(input,output_sigmoid, 'black');
% plot(input, output_cri2, 'black--');
% xlabel('z');
% ylabel('H(z)');
% legend('Sigmoid function', 'CRI (iterations=1)', 'LUT', 'Location','southeast');

% title('Sigmoid function approximation');
% hold on;
% plot(input,output_sigmoid, 'black');
% stairs(input, output_lut, 'black-.');
% legend('Sigmoid function', 'Sigmoid LUT','Location','southeast');