function [ inputs, targets ] = orl_dataset_raw
%ORL_DATASET Loads ORL faces dataset
height = 112;
width = 92;
k = width*height; % number of inputs / resolution of image
m = 400; % total number of examples

n = 40; % number of classes / number of subjects
inputs = reshape(getORLdata(255,n), m, k); % pca2d
targets = calculateLabels(m,n,1);
end

