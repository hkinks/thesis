%% initialization
clear; close all; clc

% libs
addpath('./lib');
addpath('./lib/coursera');

% dataset
[x,y] = orl_dataset_2dpca;
x = mapminmax(x);

% split data into train and validation
data_ratios = [0.8, 0.2, 0];
[trainx, testx, ~] = splitDataORL(x,40,data_ratios);
[trainy, testy, ~] = splitDataORL(y,40,data_ratios);

% image class
[r,c] = find(testy);

% options
imageindex = 28;
input = testx(imageindex,:)';
filename = strcat('image', int2str(c(imageindex)));

coefGenerate(input, filename);

fprintf('wrote the coeficients into file "%s" for image nr %d\n', filename, c(imageindex));
