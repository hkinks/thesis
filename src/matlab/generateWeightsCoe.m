%% initialization
clear; close all; clc

% libs
addpath('./lib');
addpath('./lib/coursera');

% input
load('weights_pruned.mat');

% options
input = newweights;
filename = 'weights_pruned';

coefGenerate(input, filename);
