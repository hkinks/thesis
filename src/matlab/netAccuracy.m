%% Initialization
clear; close all; clc
addpath('./lib');

drawplots = 0;
showimages = 0;
fe = 1;
n_experiments = 10;

height = 112;
width = 92;

if (fe == 1)
    dim = 2;
else 
    dim = width;
end

k = dim*height; % number of inputs / resolution of image
n = 40; % number of classes / number of subjects
m = 10*n; % total number of examples

n_layers = 1; % number of hidden layers

% hidden units
h = [40]; % hidden units

data_ratios_man = [0.8, 0.2, 0]; % let's divide data for custom validation 
data_ratios_matlab = [1.0, 0, 0]; % training; test; validation 

% get data
y = calculateLabels(m,n,1);
if ( fe == 1 )
    X = reshape(PCA_2D(getORLdata(255,n),dim), m, k); % pca2d
else
    X = reshape(getORLdata(255,n),m,height*width);   
end

% split data into training, testing and validation datasets
[trainX, validX, ~] = splitDataORL(X,n,data_ratios_man);
[trainy, validy, ~] = splitDataORL(y,n,data_ratios_man);

% Show images
if (showimages == 1)
    D = reshape(getORLdata(255,n),m,height*width);
    displayData(D(1:m, :), width);
end

accuracies = zeros(n_experiments,1);
trainTimes = zeros(n_experiments,1);
for j=1:n_experiments
    fprintf('%d. run:\n', j);
    % randomize
    perm = randperm(size(trainX,1));
    trainX = trainX(perm,:);
    trainy = trainy(perm,:);

    perm = randperm(size(validX,1));
    validX = validX(perm,:);
    validy = validy(perm,:);

    layers = (2:n_layers+1);
    net = patternnet(layers);
    net.divideFcn = 'dividerand';
    net.divideParam.trainRatio = data_ratios_matlab(1);
    net.divideParam.testRatio = data_ratios_matlab(2);
    net.divideParam.valRatio = data_ratios_matlab(3);
    net.performFcn = 'mse';
    net.trainFcn = 'trainscg';
%     net.layers{1}.transferFcn = 'logsig';
%     net.layers{2}.transferFcn = 'logsig';
    net.inputs{1}.processFcns = {'mapminmax'};
    net.outputs{2}.processFcns = {'mapminmax'};
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'tansig';
    net.trainParam.min_grad = 1e-10;
    net.trainParam.epochs = 500;
    net.trainParam.max_fail = 6;
    net.trainParam.showWindow = 1;
    
    for i=1:n_layers
       net.layers{i}.size = h(i); 
    end
    
    tic;
    [net, tr] = train(net,trainX',trainy');
    trainTimes(j) = toc;
    fprintf('\tTraining time: %.2f\n', trainTimes(j));
    
    output = net(validX');
    [errors, ~] = confusion(validy', output);
    accuracies(j) = 100*(1-errors);
    fprintf('\tValidation accuracy: %.2f%%\n', accuracies(j));
end
fprintf('======================\nAverage:\n');
fprintf('\tTime: %.2f s\n', mean(trainTimes));
fprintf('\tAccuracy: %.2f %%\n', mean(accuracies));