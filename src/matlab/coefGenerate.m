function res = coefGenerate(input, filename)

    % constants
    BIT_WIDTH = 8;
    FRACTION = 4;

    dir = 'coef';
    fullPath = strcat(dir,'\');
    fullPath = strcat(fullPath, filename);
    coeFilePath = strcat(fullPath, '.coe');
    mifFilePath = strcat(fullPath, '.mif');
    coefFile = fopen(coeFilePath, 'w');
    mifFile = fopen(mifFilePath, 'w');

    % convert to fixed point
    fiInput = fi(input, 1, BIT_WIDTH, FRACTION);
    hexInput = fiInput.hex;
    binInput = fiInput.bin;

    % write into file
    fprintf(coefFile, 'memory_initialization_radix=16;\n');
    fprintf(coefFile, 'memory_initialization_vector=');
    % iterate through all the inputs
    for i = 1:size(input)
        fprintf(coefFile, '%s ', hexInput(i, :));
        fprintf(mifFile, '%s\n', binInput(i, :));
        % if (hexInput(1,i) == ' ' && space == 1)
        %     fprintf(coefFile, '%s', hexInput(i));
        %     space = 0;
        % elseif (hexInput(1,i) ~= ' ')
        %     fprintf(coefFile, '%s', hexInput(i));
        %     space = 1;
        % end
    end
    fprintf(coefFile, ';\n');

    fclose('all');
end
