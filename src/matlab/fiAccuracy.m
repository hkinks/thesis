%% Initialization
clear; close all; clc

addpath('./lib');
addpath('./lib/coursera');

% Dataset
[X,y] = orl_dataset_2dpca;
X = mapminmax(X);
load('weights_transposed.mat');
weights = weights_export;

% constants
BIT_WIDTH = 8;
FRACTION = 4;

% options
imageIndex = 7;

% Neural network dimensions
layers = 2;
[~, k] = size(X);
n = size(y,2);
h = [40, n];

% Split data into training, testing and validation datasets
data_ratios = [0.8, 0.2, 0];
[trainX, testX, ~] = splitDataORL(X,40,data_ratios);
[trainy, testy, ~] = splitDataORL(y,40,data_ratios);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(weights(1:h(1) * (k + 1)), h(1), (k + 1));
Theta2 = reshape(weights((1 + (h(1) * (k + 1))):end), n, (h(1) + 1));

sum = fi(0, 1, 2*BIT_WIDTH, FRACTION);
maxn = 0;
maxi = 0;
index = imageIndex;

results = zeros(size(testy,1),1);
% iterate through the test samples
for index = 1:size(testy,1)
    m = [size(X, 2), h(1)];
    w_counter = 1;
    input = [ones(size(testX,1), 1), testX];
    outputs = zeros(h(1), 1);
    % go through layers
    for l = 1:layers
        % go through neurons
        for i = 1:h(l)
            % go through inputs
            for j=1:m(l)+1
                fiInput = fi(input(index,j),1,BIT_WIDTH, FRACTION);
                fiWeight = fi(weights(w_counter, 1), 1, BIT_WIDTH, FRACTION);

                sum = sum + fiInput * fiWeight;

                w_counter = w_counter + 1;
            end
            truncsum = fi(sum, 1, BIT_WIDTH, FRACTION);
            output = fi(1/(1+exp(-double(truncsum))), 1, BIT_WIDTH, FRACTION);
            outputs(i) = output;
            fprintf('%d,%d\n', l, i);
            if (l == layers) % when last layer
                if (output > maxn)
                    maxn = output;
                    maxi = i;
                end
            end
            sum = 0;
        end
        input = [1 outputs'];
        index = 1;
    end
end

fprintf('Classified as %d\n', maxi);
[a,b] = max(testy, [], 2);
fprintf('Actually %d\n', b(imageIndex));
close('all');
