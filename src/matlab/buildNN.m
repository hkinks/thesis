function [ cost, net ] = buildNN( p, X , y)
    h = 40;
    [m k] = size(X);
    n = size(y,2);
    
    data_ratios = [0.8, 0.2, 0]; % let's divide data for custom validation 
    [trainX, validX, ~] = splitData(X,40,data_ratios);
    [trainy, validy, ~] = splitData(y,40,data_ratios);
    
    fprintf('Gene: ');
    fprintf('Number of zeros %d\n', sum(p(1,:)==0));
%     fprintf('...\n');

    net = patternnet(h);    
    
    %Configuration is the process of setting network input and 
    %output sizes and ranges, input preprocessing settings and 
    %output postprocessing settings, and weight initialization 
    %settings to match input and target data.
    net = configure(net, trainX', trainy');
    
    net.divideFcn = 'dividerand';
    net.divideParam.trainRatio = 1;
    net.divideParam.valRatio = 0;
    net.divideParam.testRatio = 0;
    net.name = strcat('Test subject number #', int2str(rand*1000));
    net.trainFcn = 'trainscg';
    net.adaptFcn = 'adaptwb';
    net.performFcn = 'crossentropy';
    net.plotFcns = {'plotperform', 'plottrainstate', 'ploterrhist', 'plotconfusion', 'plotroc'};
    net.trainParam.epochs = 100; 
    net.trainParam.time = 10;
    net.trainParam.showWindow = 0;
    net.initFcn = '';
%     net.biasConnect = [0;0];
    net = init(net); %  calls net.initFcn to initialize the weight and bias values according to the parameter values net.initParam.
%     view(net);


    % setting the connections separately
    inputConnections = reshape(p(1,1:h*k), h, k);
    hiddenConnections = reshape(p(1,h*k+1:end), n, h);
    net.IW{1} = net.IW{1} .* inputConnections;
    net.LW{2} = net.LW{2} .* hiddenConnections;
%     net.b{1} = net.b{1} .* 0;
%     net.b{2} = net.b{2} .* 0;
    net.b{1} = zeros(h,1);
    net.b{2} = zeros(n,1);
    % training
    tic
    [net, ~] = train(net,trainX',trainy','useGPU','yes');
    trainingTime = toc;

    %evaluation
    output = net(validX');
    [errors, ~] = confusion(validy', output);
    acc = 100*(1-errors);
    numOfConnections = sum(sum(net.IW{1} ~= 0)) + sum(sum(net.LW{2} ~= 0));
    numOfZeros = sum(sum(net.IW{1} == 0)) + sum(sum(net.LW{2} == 0));
    cost = errors * 100 + trainingTime + numOfConnections/size(p,2)*100;
    fprintf('\tAcc: %.2f%%\n', acc);
    fprintf('\tCost: %.2f\n', cost);
    fprintf('\tZeros: %d\n', numOfZeros);
end