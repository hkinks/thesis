function g = tansigGradient(z)
g = 1.0-tanh(z).^2;
end
