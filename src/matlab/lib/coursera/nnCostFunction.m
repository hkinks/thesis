function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices.
%
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% zerosN = sum(sum(Theta1==0)) + sum(sum(Theta2==0));
% fprintf('\nNumber of zero connections in cost function %d\n', zerosN);

% Setup some useful variables
m = size(X, 1);

Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

%activation
z2=[ones(m,1) X]*Theta1'; % m x s2
a2=sigmoid(z2);
z3=[ones(m,1) a2]*Theta2'; % m x k
h=sigmoid(z3); % m x k
% y = zeros(m,num_labels);
temp = 0;
for i=1:m
%     fprintf('wtf %d\n',y(i));
%     y(i, y(i)) = 1; % convert to bit array
    % calculate cost
	temp = temp + (-y(i,:) * log(h(i,:))' - (1-y(i,:))*log(1-h(i,:))');
end
J=temp/m;

% Regularized
J = J + (lambda/(2*m)) * (sum(sum(Theta1(:,2:end).^2)) + ...
    sum(sum(Theta2(:,2:end).^2)));

% Gradient

D1=0;
D2=0;

for i=1:m
    %feedforward
    a1=[1 X(i,:)];
    z2=a1*Theta1';
    a2=[1 sigmoid(z2)];
    a3=sigmoid(a2*Theta2'); % 1xk

    d3=a3-y(i,:); %1xk
    d2=Theta2'*d3(:).*[1 sigmoidGradient(z2)]';  % 1x(s2+1)
    d2=d2(2:end);% m x s2


    D1=D1+d2*a1;
    D2=D2+d3'*a2;
end

Theta1_grad(:,1) = (1/m) * D1(:,1);
Theta2_grad(:,1) = (1/m) * D2(:,1);
Theta1_grad(:,2:end) = ((1/m) * D1(:,2:end) + lambda/m*Theta1(:,2:end)) .* (Theta1(:,2:end)~=0);
Theta2_grad(:,2:end) = ((1/m) * D2(:,2:end) + lambda/m*Theta2(:,2:end)) .* (Theta2(:,2:end)~=0);

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
