function [h, display_array] = displayDataWithTargets(X, example_width, indy, indp)
%DISPLAYDATA Display 2D data in a nice grid
%   [h, display_array] = DISPLAYDATA(X, example_width) displays 2D data
%   stored in X in a nice grid. It returns the figure handle h and the 
%   displayed array if requested.

% Gray Image
colormap(gray);

% Compute rows, cols
[m n] = size(X);
example_height = (n / example_width);

% Compute number of items to display
% display_rows = floor(sqrt(m));
% display_cols = ceil(m / display_rows);
display_cols = 10;
display_rows = ceil(m / display_cols);

% Between images padding
pad = 4;

% Setup blank display
display_array = - ones(pad + display_rows * (example_height + pad), ...
                       pad + display_cols * (example_width + pad));

% Copy each example into a patch on the display array
curr_ex = 1;
for j = 1:display_rows
	for i = 1:display_cols
		if curr_ex > m, 
			break; 
		end
		% Copy the patch
		% Get the max value of the patch
		max_val = max(abs(X(curr_ex, :)));
		subplot(display_rows,display_cols,curr_ex), subimage(reshape(X(curr_ex, :), example_height, example_width)/max_val);
        if indy(curr_ex) == indp(curr_ex) 
            title('1',  'FontSize', 9);
        else
            title('0',  'FontSize', 9);
        end
%         title(['Target' int2str(indy(curr_ex)) ' ' 'Prediction' int2str(indp(curr_ex))],  'FontSize', 9);
        axis image off;
% 		display_array(pad + (j - 1) * (example_height + pad) + (1:example_height), ...
% 		              pad + (i - 1) * (example_width + pad) + (1:example_width)) = ...
% 						reshape(X(curr_ex, :), example_height, example_width) / max_val;
		curr_ex = curr_ex + 1;
	end
	if curr_ex > m, 
		break; 
	end
end
drawnow;

end
