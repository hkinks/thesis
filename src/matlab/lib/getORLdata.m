function D = getORLdata(normalization,nSubjects)
% normalization - int
% nSubjects
    nSamples = 10;
    n = nSubjects*nSamples;
    
    height = 112;
    width = 92;
    
    ext = '.pgm';

    databaseORLPath = 'datasets\ORL\';
    
    D = zeros(n, height, width);

    for s = 1:nSubjects
        for i = 1:nSamples
            img = imread(strcat(databaseORLPath,'s',int2str(s),'\',int2str(i),ext));
            img = double(img)/normalization;
            D((s-1)*nSamples+i,:,:) = img;
        end
    end
end

