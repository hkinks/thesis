function [ train, test, valid ] = splitDataORL( X, n, ratios )
    % split data into training, testing and validation datasets
    % taking into consideration the ORL data set
    train = zeros(ratios(1)*size(X,1), size(X,2));
    test = zeros(ratios(2)*size(X,1), size(X,2));
    valid = zeros(ratios(3)*size(X,1), size(X,2));

    trainCount = 1;
    testCount = 1;
    validCount = 1;

    for i=1:n
       for j=1:ratios(1)*10
           train(trainCount, :) = X((i-1)*10+j, :);
           trainCount = trainCount + 1;
       end
       for jj=j+1:j+ratios(2)*10
           test(testCount, :) = X((i-1)*10+jj, :);
           testCount = testCount + 1;
       end
       for jjj=jj+1:jj+ratios(3)*10
           valid(validCount, :) = X((i-1)*10+jjj, :);
           validCount = validCount + 1;
       end
    end
end

