function [ train, test, valid ] = splitData( X, ratios )
    m = size(X,1); % how many samples

    trainSize = floor(ratios(1) * m);
    testSize= floor(ratios(2) * m);
%     validSize = m - trainRatio - testRatio;

    % split data into training, testing and validation datasets
%     train = zeros(trainSize, size(X,2));
%     test = zeros(testSize, size(X,2));
%     valid = zeros(validSize, size(X,2));
    
    train = X(1:trainSize, :);
    test = X(trainSize+1 : trainSize+testSize, :);
    valid = X(trainSize+testSize+1:end, :);
end

