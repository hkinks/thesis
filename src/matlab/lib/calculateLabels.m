function y = calculateLabels(m, n, bit_vector)
% calculateLabels  Calculate expected label values. Meant to be used with
% ORL datatset.
%   m - total number of examples in the dataset. Default 400
if bit_vector == 1
    y = zeros(m, n);
    for i = 1:m
        y(i, floor((i-1)/10) + 1) = 1;
    end
else
    y = zeros(m,1);
    for i = 1:m
        y(i) = floor((i-1)/10) + 1;
    end
end
end