addpath('./lib');
n_experiments = 1;

height = 112;
width = 92;
dim = 2;
k = dim*height; % number of inputs / resolution of image
n = 40; % number of classes / number of subjects
m = 10*n; % total number of examples

gene = {[2 40 40 1 1 1 1 0 0 1 0 1 0];
        [2 40 40 1 1 2 4 1 1 1 1 0 0];
        [2 40 40 1 1 2 4 0 0 1 0 0 0];
        [1 40 1 4 0 0 1];
        [1 40 1 4 0 0 0]};
examples = size(gene,1);
    
y = calculateLabels(m,n,1);
X = reshape(PCA_2D(getORLdata(255,n),dim), m, k); 
% rawX = reshape(getORLdata(255,n),m,height*width);
% X = rawX;
data_ratios = [0.8, 0.2, 0]; % let's divide data for custom validation 
[trainX, validX, ~] = splitData(X,40,data_ratios);
[trainy, validy, ~] = splitData(y,40,data_ratios);

acc=zeros(n_experiments,examples);
for j = 1:examples
    for i=1:n_experiments
        [~, ganet] = buildAndTrainNN(gene{j}, X, y, gene{j}(1));
        acc(i,j) = 100*(1-confusion(validy',ganet(validX')));
%         view(ganet);
    end
end
mean(acc,1)
