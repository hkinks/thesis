%% Initialization
clear; close all; clc

addpath('./lib');
addpath('./lib/coursera');

% Dataset
[X,y] = orl_dataset_2dpca;
% [D,y] = digittrain_dataset; D=D';y=y';
% X = zeros(size(y,1), size(D{1},1)^2);
% for i=1:size(D,1)
%    X(i,:) = reshape(D{i},1,size(D{i},1)^2);
% end
% Iris dataset
% [X,y] = iris_dataset; X=X';y=y';
X = mapminmax(X);
% options
drawplots = 1;
printLog = 1;
epochs = 20;
gradient_iterations = 20;
options = optimset('MaxIter', gradient_iterations);
pruneThreshold = 0.1;

% Lambda - regularization parameter. For controlling overfittinpg
lambda = 0.4; % can be added to gene later on

% Neural network dimensions
h = 40;
[~, k] = size(X);
n = size(y,2);

%% Training neural network
% random weight initialization
initial_Theta1 = randInitializeWeights(k, h);
initial_Theta2 = randInitializeWeights(h, n);

% Unroll parameters
weights = [initial_Theta1(:) ; initial_Theta2(:)];

% randomize
%     data = [X y];
%     perm = randperm(size(data,1));
%     data = data(perm,:);
%     X = data(:,1:k);
%     y = data(:, k+1:end);

% Split data into training, testing and validation datasets
data_ratios = [0.8, 0.2, 0];
% [trainX, testX, ~] = split(X,data_ratios);
% [trainy, testy, ~] = split(y,data_ratios);
[trainX, testX, ~] = splitDataORL(X,40,data_ratios);
[trainy, testy, ~] = splitDataORL(y,40,data_ratios);

% randomize
perm = randperm(size(trainX,1));
trainX = trainX(perm,:);
trainy = trainy(perm,:);

perm = randperm(size(testX,1));
testX = testX(perm,:);
testy = testy(perm,:);

trainAcc = zeros(epochs,1);
testAcc = zeros(epochs,1);

costFunction = @(p) nnCostFunction(p, k, h, n, trainX, trainy, lambda);
tic % measure training time
for i = 1:epochs
    % Train
    [nn_params, ~] = fmincg(costFunction, weights, options);

    % Obtain Theta1 and Theta2 back from nn_params
    Theta1 = reshape(nn_params(1:h * (k + 1)), h, (k + 1));
    Theta2 = reshape(nn_params((1 + (h * (k + 1))):end), n, (h + 1));

    %% Evaluate
    % Get the predictions of trained network
    trainPred0 = predict(Theta1, Theta2, trainX);
    testPred0 = predict(Theta1, Theta2, testX);
    % Find the class with highest value
    trainPred = compet(trainPred0')';
    testPred = compet(testPred0')';
    % Get accuracy
    trainAcc(i) = mean(all(trainPred == trainy,2)) * 100;
    testAcc(i) = mean(all(testPred == testy,2)) * 100;
    weights = [Theta1(:) ; Theta2(:)];

    %% Plotting
    if drawplots == 1
            % Find if figure already open
            fig1 = findobj('type','figure','name','fig1');
            if isempty(fig1)
                figure('name', 'fig1');
            else
                clf(fig1); % clear the figure
                set(0, 'CurrentFigure', fig1); % select the figure
            end

            % error plot
            hold all;
            plot(1:i, 100-trainAcc(1:i), 'black');
            plot(1:i, 100-testAcc(1:i), 'black--');
            axis([1,epochs, 0, 100]);
            xlabel('Epochs');
            ylabel('Error %');
            legend('Train', 'Test');

            indexmin = find(min(testError) == testError);
            ymin = testError(indexmin(end));
            strmin = [num2str(ymin)];
            text(indexmin(end),ymin,[strmin ' %'],'HorizontalAlignment','left', 'VerticalAlignment', 'top');
            axis([1,epochs, 0, 100]);
            drawnow;
    end
end
trainingTime = toc;
prepruneWeights = weights;


%% Log
if printLog == 1
    fprintf('Training time %.2f\n', trainingTime);
    fprintf('Testing with lambda %.2f\n', lambda);
    fprintf('Train acc: %f \tTest acc: %f\n', max(trainAcc), max(testAcc));
%         fprintf('Gradient cost: %.2f\n', grad_cost);
    fprintf('------------------\n');
end

%% Prune
weightMask = abs(weights) > pruneThreshold;
totalConn = size(weights,1);
connections = sum(weightMask);

% random weight initialization
% initial_Theta1 = randInitializeWeights(k, h);
% initial_Theta2 = randInitializeWeights(h, n);

% Unroll parameters
weights = [initial_Theta1(:) ; initial_Theta2(:)];
weights = weightMask .* weights;

Theta1 = reshape(weights(1:h * (k + 1)), h, (k + 1));
Theta2 = reshape(weights((1 + (h * (k + 1))):end), n, (h + 1));
% Get the predictions of trained network
trainPred = predict(Theta1, Theta2, trainX);
testPred = predict(Theta1, Theta2, testX);
% Find the class with highest value
trainPred = compet(trainPred')';
testPred = compet(testPred')';
% Get accuracy
trainAcc = mean(all(trainPred == trainy,2)) * 100;
testAcc = mean(all(testPred == testy,2)) * 100;



for i = 1:epochs
    % Train
    [nn_params, ~] = fmincg(costFunction, weights, options);

    % Obtain Theta1 and Theta2 back from nn_params
    Theta1 = reshape(nn_params(1:h * (k + 1)), h, (k + 1));
    Theta2 = reshape(nn_params((1 + (h * (k + 1))):end), n, (h + 1));

    % Evaluate
    % Get the predictions of trained network
    trainPred = predict(Theta1, Theta2, trainX);
    testPred = predict(Theta1, Theta2, testX);
    % Find the class with highest value
    trainPred = compet(trainPred')';
    testPred = compet(testPred')';
    % Get accuracy
    trainAcc(i) = mean(all(trainPred == trainy,2)) * 100;
    testAcc(i) = mean(all(testPred == testy,2)) * 100;
    weights = [Theta1(:) ; Theta2(:)];

    % Plotting
    if drawplots == 1
        % Find if figure already open
        fig1 = findobj('type','figure','name','fig1');
        set(0, 'CurrentFigure', fig1); % select the figure

        % error plot
        hold all;
        plot(1:i, 100-trainAcc(1:i), 'r');
        plot(1:i, 100-testAcc(1:i), 'r--');
        axis([1,epochs, 0, 100]);
        legend('Train','Test','Pruned train', 'Pruned test');
        drawnow;
    end
end

%% Log
if printLog == 1
    fprintf('Cutting connections below %.2f\n', pruneThreshold);
    fprintf('Connected %.2f%%\n', connections/totalConn*100);
    fprintf('Training time %.2f\n', trainingTime);
    fprintf('Testing with lambda %.2f\n', lambda);
    fprintf('Train acc: %f \tTest acc: %f\n', max(trainAcc), max(testAcc));
%         fprintf('Gradient cost: %.2f\n', grad_cost);
    fprintf('------------------\n');
end

weights_export = weights;
