function g = sigmoidLUT(z, bit_width, range)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.
if ~exist('bit_width', 'var')
    bit_width = 4;
end

if ~exist('range', 'var')
    L = 6;
else
    L = range;
end

g = zeros(size(z,1), size(z,2));
    for i=1:max(size(z,1))
        for k=1:max(size(z,2))
            if ( z(i,k) <= -L )
                g(i,k) = 0;
            elseif ( z >= L )
                g(i,k) = 1;
            else
                for j=-L:(2*L)/(2^bit_width):L
                    if z(i,k) < j
%                         g(i,k) = fi(sigmoid(j), 1, 8, 4);
                        % g(i,k) = sigmoid(j);
                        g(i,k) = sigmoid(j);
                        break;
                    end
                end
            end
        end
    end
end
