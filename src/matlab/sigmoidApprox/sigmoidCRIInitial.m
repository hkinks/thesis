function g = sigmoidCRIInitial(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.
    if ( z >= 2 )
        g = 1;
    elseif (z <= -2 )
        g = 0;
    else
        g = 1/2*(1+z/2);
    end
end
