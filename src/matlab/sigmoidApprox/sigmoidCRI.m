function [ output ] = sigmoidCRI( x, q )
output = zeros(size(x));
for row = 1:size(x,1)
    for col = 1:size(x,2)
        if x(row,col)<0.5
            g=0;
            h=(1/2)*(1+x(row,col)/2);
            d=0.3;
            for i=1:q
                g_=max([g h]);
                h=(1/2)*(g+h+d);
                g=g_;
                d=d/4;
            end
            output(row,col) = max([g h]);
        else
            g=1;
            h=(1/2)*(1+x(row,col)/2);
            d=0.3;
            for i=1:q
                g_=min([h g]);
                h=(1/2)*(g+h-d);
                g=g_;
                d=d/4;
            end
            output(row,col) = min([h g]);
        end
    end
end
end

