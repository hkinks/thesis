import numpy as np


def normalize(X, low, high, dtype=None):
    """
    Normalizes the data in given range
    :param X: input data to be normalized.
    :param low: minimum value
    :param high: maximum value
    :param dtype: data type
    :return: normalized array
    """
    X = np.asarray(X)
    minx, maxx = np.min(X), np.max(X)
    # normalize to [0...1].
    X -= float(minx)
    X /= float((maxx - minx))
    # scale to [ low ... high ].
    X = X * (high - low)
    X = X + low
    if dtype is None:
        return np.asarray(X)
    return np.asarray(X, dtype=dtype)


def project(W, X, mu=None):
    """
    Project input image to eigenfaces orthonormal basis.
    http://hkinks.com/upload/db734e.png
    :param W: eigenvector. Image dimensions are in cols. (e.g. 2576x40)
    :param X: input data, face image. Array.
    :param mu: mean
    :return: vector of projections. Coefficients that can be used to recreate face using the eigenfaces
    """
    if mu is None:
        return np.dot(W, X)
    return np.dot(W, X - mu.T)


def reconstruct(W, Y, mu=None):
    """
    :param W: eigenvector
    :param Y: parameter vector/ coefficients
    :param mu: mean
    :return: image reconstructed from the coefficients
    """
    if mu is None:
        return np.dot(W.T, Y)
    return np.dot(W.T, Y) + mu.T


def pca(X, dimensions=0):
    """
    Principal Component Analysis.
    Taken from: http://www.bytefish.de/pdf/facerec_python.pdf
    :param X: input data
    :param dimensions: number of components
    :return [eigenvalues, eigenvectors, mean]
    """
    [nn, d] = X.shape
    if (dimensions <= 0) or (dimensions > nn):
        dimensions = nn
    mu = X.mean(axis=0)
    X = X - mu
    if nn > d:
        C = np.dot(X.T, X)
        [eigenvalues, eigenvectors] = np.linalg.eigh(C)
    else:
        C = np.dot(X, X.T)
        [eigenvalues, eigenvectors] = np.linalg.eigh(C)
        eigenvectors = np.dot(X.T, eigenvectors)
        for i in xrange(nn):
            eigenvectors[:, i] = eigenvectors[:, i] / np.linalg.norm(eigenvectors[:, i])

    # sort eigenvectors descending by their eigenvalue
    idx = np.argsort(-eigenvalues)
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]

    # select only num_components
    eigenvalues = eigenvalues[0: dimensions].copy()
    eigenvectors = eigenvectors[:, 0:dimensions].copy(44)
    return [eigenvalues, eigenvectors, mu]


def pca2d(A, dimensions=0):
    """
    Two-Dimensional Principal Component Analysis. Based on Sai's matlab code.
    :param X: input data. Image, k x m x n matrix, where m - height, n - width, k - sample.
    :param dimensions:
    :return:
    """
    # TODO: doesn't seem to perform correct operation
    [k, m, n] = A.shape

    # mean
    mu = A.mean(axis=0)
    # duplicating matrix k times
    mu = np.tile(mu, (k, 1, 1))

    # removing mean from faces
    dif = A - mu

    L = np.zeros((n, n))

    for i in range(k):
        L = L + np.dot(dif[i, :, :].T, dif[i, :, :])

    L = L / k

    [eigenvalues, eigenvectors] = np.linalg.eig(L)

    i = np.argsort(-eigenvalues)
    eigenvalues = eigenvalues[i]
    eigenvectors = eigenvectors[:, i]

    # only numerical components of significant eigenvectors
    eigenvalues = eigenvalues[0:dimensions].copy()
    eigenvectors = eigenvectors[:, 0:dimensions].copy()

    # #testing
    # print eigenvectors.shape
    # print L.shape
    # projection = np.dot(A[11, :, :], eigenvectors)
    # print projection.shape
    # reconstruction = np.zeros((56, 46))
    # for i in range(2):
    #     reconstruction = np.dot(projection, eigenvectors[:, :].T)
    #
    # from PIL.Image import fromarray
    # # fromarray(normalize(projection[:, :], 0, 255)).show()
    # fromarray(normalize(reconstruction[:, :], 0, 255)).show()

    return [eigenvalues, eigenvectors, mu]
