from os.path import dirname, abspath, join

import numpy as np

import feature_extraction
import util

# PyBrain for Neural Networks
from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer

from pybrain.tools.xml.networkwriter import NetworkWriter

# Pillow. For image operations
from PIL.Image import open

# CONSTANTS

# input data dimensions
HEIGHT = 56
WIDTH = 46
RESOLUTION = HEIGHT * WIDTH
O_DIM = 2  # reduced dimensions

# number of examples
N_SUBJECTS = 40
N_IMG = 10
N_TOTAL_IMAGES = N_SUBJECTS * N_IMG

# ANN
TEST_TRAIN_RATIO = 0.60  # percentage of the data split for training and testing
EPOCHS = 200
HIDDEN_NEURONS = 40

# export information
FEAT_EX = "2dpca" # 2dpca | pca
EXPERIMENT = FEAT_EX + "_d%d_e%d_h%d" % (O_DIM, EPOCHS, HIDDEN_NEURONS) # experiment name

# export files
NETWORK_FILE = "results/" + EXPERIMENT + "/network.xml"
CSV_FILE = "results/" + EXPERIMENT + "/result.csv"
PLOT_FILE = "results/" + EXPERIMENT + "/result.png"


def main():
    # read face image data from ORL database
    util.print_title("Reading in ORL dataset")
    data = read()

    # Principal Component Analysis. Returns eigenfaces
    util.print_title("Performing " + FEAT_EX)

    if FEAT_EX == "2dpca":
        # converting data to three dimensional for 2dpca
        data3d = np.zeros((N_TOTAL_IMAGES, HEIGHT, WIDTH))
        for i in range(N_TOTAL_IMAGES):
            data3d[i, :, :] = np.reshape(data[:, i], (HEIGHT, WIDTH))

        [eigenval, eigenvec, mu] = feature_extraction.pca2d(data3d, dimensions=O_DIM)

        P = np.zeros((N_TOTAL_IMAGES, HEIGHT, O_DIM))
        for i in range(N_TOTAL_IMAGES):
            P[i, :, :] = np.dot(data3d[i, :, :], eigenvec)

        print("Got projection with shape %s" % str(P.shape))
        P = P.reshape((N_TOTAL_IMAGES, HEIGHT * O_DIM)).T
        print("Reshaped it to %s" % str(P.shape))
    elif FEAT_EX == "pca":
        [Null, eigenvec, mu] = feature_extraction.pca(data.T, dimensions=O_DIM)
        # util.visualize_stuff(data, eigenvec, mu)
        # return
        P = feature_extraction.project(data.T, eigenvec).T
        print P.shape

    # Training
    fnn = classify(P)

    # genetic algorithm TODO
    # - Genetic algorithm (Fully Connected NN):
    # - number of hidden layers (0 to 2)
    # - number of neurons per layer
    # - activation function types per layer
    # - learning Alg.
    # - number of training cycles per individual
    # - seed control

    # Write the network to file
    NetworkWriter.writeToFile(fnn, NETWORK_FILE)

    # test
    # test_trained_nn(P, fnn)
    return


def test_trained_nn(data, fnn):
    """
    Additional testing of results. Written as there were problems with PyBrains percentage errors.
    :param data: Image data
    :param fnn: Trained pybrain's neural network
    """
    correct_count = 0
    step = 10
    for img in range(0, N_TOTAL_IMAGES, step):
        out = fnn.activate(data[:, img])
        out = out.argmax()  # the highest output activation gives the class
        correct = int(np.floor(img / 10))
        if correct == out:
            correct_count += 1
            # print("Image nr %d classified as %d (correct %d)." % (img, out, correct))
    percentage = 100. * float(correct_count) / float(N_TOTAL_IMAGES / step)
    print("Correct count %d (%.2f%%)" % (correct_count, percentage))


def classify(data, hidden_neurons=HIDDEN_NEURONS, momentum=0.0, weight_decay=0.01, learning_rate=0.01, learning_rate_decay=1,
             epochs=EPOCHS):
    """
    PyBrain Neural Net classifier
    """
    util.print_title("Starting classification with ANN.")
    print("ANN input data dimensionality: %s" % str(data.shape))
    # get the  dimensionality of the input
    dim = np.size(data, 0)

    # assuming classes as the subjects of faces
    alldata = ClassificationDataSet(dim, 1, nb_classes=N_SUBJECTS)

    # add samples to datasets
    for i in range(N_TOTAL_IMAGES):
        # adding subject number as a target
        subject = int(np.floor(i / 10))
        alldata.addSample(data[:, i].ravel(), [subject])

    # When doing classification, many algorithms work better if classes are
    # encoded into one output unit per class

    trndata, tstdata = alldata.splitWithProportion(TEST_TRAIN_RATIO)

    trndata._convertToOneOfMany()
    tstdata._convertToOneOfMany()

    print("Building network")
    print("Number of training patterns: %d" % len(trndata))
    print("Input dimensions: " + str(trndata.indim))
    print("Output dimensions: " + str(trndata.outdim))
    # for i in range(0, int(n * test_train_ratio), 100):
    #     print("%dth sample class:" % i)
    #     print trndata['input'][i], trndata['class'][i]

    fnn = buildNetwork(trndata.indim, hidden_neurons, trndata.outdim, outclass=SoftmaxLayer)

    trainer = BackpropTrainer(fnn, dataset=trndata, verbose=False,
                              momentum=momentum, weightdecay=weight_decay, learningrate=learning_rate,
                              lrdecay=learning_rate_decay)
    util.print_title("Starting training")

    results = []
    for i in range(epochs):
        # trainer.trainEpochs(epochs)
        trainer.train()
        trnresult = percentError(trainer.testOnClassData(),
                                 trndata['class'])

        tstresult = percentError(trainer.testOnClassData(
            dataset=tstdata), tstdata['class'])
        if i % 10 == 0:
            print("epoch: %4d" % trainer.totalepochs,
                  " train error: %.2f%%" % trnresult,
                  " test error: %.2f%%" % tstresult)

        results.append(tstresult)

    util.plot_result(range(epochs), results, PLOT_FILE, title=EXPERIMENT)
    util.save_result(range(epochs), results, CSV_FILE)
    return fnn


def read(normalize=True, height=HEIGHT, width=WIDTH):
    """
    Read face image data from the ORL database. The matrix's shape is
    46*56 (pixels) x 400 (faces).
    Step through each subject and each image. Reduce the size of the images
    by a factor of 0.5.

    @return the ORL faces data matrix with shape (height*width x n )
    """
    print("Reading ORL faces database")
    print("Matrix shape: %d x %d" % (height, width))
    print("Examples: %d (%d subjects and %d images)" % (N_TOTAL_IMAGES, N_SUBJECTS, N_IMG))
    dir = join(dirname(dirname(abspath(__file__))), 'datasets', 'ORL', 's')
    V = np.matrix(np.zeros((width * height, N_TOTAL_IMAGES)))
    for subject in range(N_SUBJECTS):
        for image in range(N_IMG):
            im = open(join(dir + str(subject + 1), str(image + 1) + ".pgm"))
            # reduce the size of the image
            im = im.resize((width, height))
            V[:, subject * 10 + image] = np.mat(np.asarray(im).flatten()).T
    if normalize:
        V /= 255.
    return V


if __name__ == "__main__":
    main()
