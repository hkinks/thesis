""" For learning purposes """
__author__ = 'hannes'
import numpy as np
from warnings import warn
from matplotlib import pyplot as plt
try:
    from matplotlib.pyplot import savefig, imshow, set_cmap
except ImportError as exc:
    warn("Matplotlib must be installed.")

try:
    from PIL.Image import open, fromarray, new
    from PIL.ImageOps import expand
except ImportError as exc:
    warn("PIL must be installed.")

def run():
    mu = np.array([0,0])
    sigma = np.array([[0,10],[100,0]])
    M = np.random.multivariate_normal(mu, sigma, 400).T

    mean_x = np.mean(M[0,:])
    mean_y = np.mean(M[1,:])

    # plot 1
    plt.subplot(2,1,1)
    plt.title("Datapoints")

    plt.plot(M[0,:],M[1,:], 'bo', alpha=0.8, zorder=1)
    plt.plot(mean_x, mean_y, 'ro', zorder=3)

    plt.xlim([-20,20])
    plt.ylim([-10,10])


    # covariance
    d = 2
    cov = np.matrix(np.zeros((d,d)))
    for i in range(d):
        for j in range(d):
           cov[i,j] = np.mean(np.multiply(M[j,:] - np.mean(M[i,:]), M[i,:] - np.mean(M[j,:])))
    eig_val, eig_vec = np.linalg.eig(cov)

    vec1_x1 = mean_x
    vec2_x1 = mean_x
    vec1_y1 = mean_y
    vec2_y1 = mean_y
    vec1_dx2 = eig_vec[0,0] * eig_val[0]/10
    vec1_dy2 = eig_vec[0,1] * eig_val[0]/10
    vec2_dx2 = eig_vec[1,0] * eig_val[1]/10
    vec2_dy2 = eig_vec[1,1] * eig_val[1]/10


    plt.arrow(vec1_x1, vec1_y1, vec1_dx2, vec1_dy2, head_width=1, head_length=2, fc='k', ec='k', width=0.5, zorder=2)
    plt.arrow(vec2_x1, vec2_y1, vec2_dx2, vec2_dy2, head_width=1, head_length=2, fc='k', ec='k', width=0.5, zorder=2)

    # normal distribution histogram
    plt.subplot(2,1,2)
    plt.title("Difference from mean")
    dif_x = M[0,:]-mean_x
    dif_y = M[1,:]-mean_y
    plt.hist([dif_x,dif_y], 40, normed=0, color=['g','r'], alpha=0.50)

    plt.show()


if __name__ == "__main__":
    run()
