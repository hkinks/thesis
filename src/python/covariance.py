__author__ = 'hannes'
import numpy as np
from warnings import warn
from matplotlib import pyplot as plt

try:
    from PIL.Image import open, fromarray, new
    from PIL.ImageOps import expand
except ImportError as exc:
    warn("PIL must be installed.")


def run():
    M = create_data()

    mean_x = np.mean(M[0,:])
    mean_y = np.mean(M[1,:])

    # plot 1
    plt.subplot(2,1,1)
    plt.title("Datapoints")

    plt.plot(M[0,:],M[1,:], 'bo')
    plt.plot(mean_x, mean_x, 'ro')

    plt.xlim([-30,30])
    plt.ylim([-30,30])

    # plot2
    plt.subplot(2,1,2)
    plt.title("Difference from mean")
    dif_x = M[0,:]-mean_x
    dif_y = M[1,:]-mean_y
    plt.hist([dif_x,dif_y], 40, normed=0, color=['g','r'], alpha=0.50)

    # covariance
    d = 2
    cov = np.matrix(np.zeros((d,d)))
    for i in range(d):
        for j in range(d):
           cov[i,j] = np.mean(np.multiply(M[j,:] - np.mean(M[i,:]), M[i,:] - np.mean(M[j,:])))
    eig_val, eig_vec = np.linalg.eig(cov)
    eig_vec = eig_vec.round()
    eig_val = eig_val.round()
    print eig_val
    print eig_vec

    plt.show()


def create_data(s1=10, s2=150, n=400):
    mu = np.array([0, 0])
    sigma = np.array([[0, s1], [s2, 0]])
    M = np.random.multivariate_normal(mu, sigma, n).T
    return M


def nntest():
    M = create_data().T
    n = np.shape(M)[0]
    X = np.hstack((np.ones(n).reshape(n,1), M[:, 4]))
    pass


if __name__ == "__main__":
    # run()
    nntest()
