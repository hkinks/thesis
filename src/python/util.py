from os import path, makedirs
from matplotlib import cm as cm
import matplotlib.pyplot as plt
import numpy as np
import feature_extraction
import csv


def visualize_stuff(data, eigenvec, mu, height=56, width=46):
    """
    Visualizes faces on a plot.
    """
    images = []
    titles = []
    # plot one very mean face
    images.append(mu.reshape(height, width))
    titles.append("Mean face")
    # plot eigenfaces
    for i in range(10):
        titles.append("eigenface %d" % i)
        images.append(eigenvec[:, i].reshape(height, width))

    # reconstruction of faces from eigenvectors
    for i in range(0, 200, 10):
        W = eigenvec[:, 0:i]
        #     # project a face onto our 'face space'
        #     # get projection W0, W1, ... Wm
        #     # W_k = U^T_k (\Gamma - \phi) ( http://www.ijcte.org/papers/213-H282.pdf p.256)
        P = feature_extraction.project(W, data[:, 0], mu)
        R = feature_extraction.reconstruct(W, P, mu)
        R = feature_extraction.normalize(R, 0, 255, dtype=np.uint8).T
        images.append(R.reshape(height, width))
        titles.append("step %d" % i)
    plot_img(images, 6, 6, "Faces on different steps", titles=titles)


def plot_img(images, rows, cols, maintitle="Plot", titles=[], colormap=cm.gray, filename=None):
    fig = plt.figure()
    fig.text(.5, .95, maintitle, horizontalalignment='center')
    for i in xrange(len(images)):
        ax0 = fig.add_subplot(rows, cols, (i + 1))
        plt.axis('off')
        if len(titles) == len(images):
            plt.title(titles[i], size=9)

        plt.imshow(np.asarray(images[i]), cmap=colormap)
    if filename is None:
        plt.show()
    else:
        fig.savefig(filename)


def print_title(title):
    print("\n### %s ###" % title)


def mkdir_if_needed(file):
    if not path.exists(path.dirname(file)):
        makedirs(path.dirname(file))


def plot_result(epoch, tstresult, file, title=None):
    plt.plot(epoch, tstresult)
    if title:
        plt.title(title)
    plt.axis([0, 10, 0, 100])
    plt.autoscale(enable=True, axis='x')
    plt.xlabel("Epochs")
    plt.ylabel("Error percentage (%)")

    mkdir_if_needed(file)
    print("Writing plot to file %s." % str(file))
    plt.savefig(file)
    # plt.show()


def save_result(epoch, tstresult, file):
    mkdir_if_needed(file)
    print("Saving results to file %s." % str(file))
    with open(file, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for i in range(epoch[-1]):
            writer.writerow([epoch[i], tstresult[i]])
