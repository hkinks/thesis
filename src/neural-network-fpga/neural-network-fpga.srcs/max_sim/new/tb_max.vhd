library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;

entity tb_max is
end tb_max;

architecture behavioral of tb_max is
component max is
    port (
        ena : in std_logic;
        x : in std_logic_vector(BIT_WIDTH-1 downto 0);
        y : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end component;

signal ena : std_logic;
signal x : std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
signal y : std_logic_vector(BIT_WIDTH-1 downto 0);

begin
    uut: max port map(ena, x, y);

    stim_proc : process begin
        wait for 5 ns;
        ena <= '1';
        wait for 5 ns;

        for i in 0 to 255 loop
            x <= std_logic_vector(signed(x) + 1);
            wait for 5 ns;
        end loop;

        for i in 0 to 255 loop
            x <= std_logic_vector(unsigned(x) - 1);
            wait for 5 ns;
        end loop;

        wait for 5 ns;
        ena <= '0';
        wait for 5 ns;
        assert (false) report "end" severity failure;
    end process;
end behavioral;
