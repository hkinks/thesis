library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;

entity logsig_tb is
end logsig_tb;

architecture behavioral of logsig_tb is
    component logsig
        port (
            clk : in std_logic;
            enable : in std_logic;
            input : in std_logic_vector(BIT_WIDTH-1  downto 0);
            output : out std_logic_vector(BIT_WIDTH-1  downto 0)
        );
    end component;

	signal clk_tb : std_logic := '1';
    signal ena_tb : std_logic := '0';
	signal input_tb : std_logic_vector(BIT_WIDTH-1  downto 0) := "10000000";
	signal output_tb : std_logic_vector(BIT_WIDTH-1  downto 0);

	signal next_value : std_logic_vector(BIT_WIDTH-1  downto 0) := "10000000";

    constant period : time := 10 ns;
begin
    uut: logsig port map (
        clk => clk_tb,
        enable => ena_tb,
        input => input_tb,
        output => output_tb
    );

    next_value <= std_logic_vector( unsigned(input_tb) + 1 );
    clk_tb <= not clk_tb after 5 ns;

    stim_proc : process
    begin
        wait for period;
        ena_tb <= '1';
        wait for period;
        for i in 0 to 255 loop
            wait for period;
            input_tb <= next_value;
        end loop;

        wait for period;

        assert (false) report "end" severity failure; -- to stop sim
    end process;
end behavioral;
