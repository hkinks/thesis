library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity neuron_tb is
end neuron_tb;

architecture behavioral of neuron_tb is
    constant PERIOD : time := 10 ns;

	component neuron is
	    port(
	    	clk : in std_logic;
	    	res : in std_logic;
	    	ena : in std_logic;
	    	x : in std_logic_vector(BIT_WIDTH-1 downto 0); 
	    	w : in std_logic_vector(BIT_WIDTH-1 downto 0); 
	        y : out std_logic_vector(BIT_WIDTH-1 downto 0)
	    );
	end component;
		
	signal clk_tb : std_logic := '0';
	signal res_tb : std_logic := '0';
	signal ena_tb : std_logic := '0';
	signal input_tb : std_logic_vector(BIT_WIDTH-1 downto 0);
	signal weight_tb : std_logic_vector(BIT_WIDTH-1 downto 0);
    signal output_tb : std_logic_vector(BIT_WIDTH-1 downto 0);

	signal next_value : std_logic_vector(BIT_WIDTH-1  downto 0) := "10000000";

	-- sample input and weights signal 
	signal weight_1 : sfixed(A downto -B) := to_sfixed(0.3, A, -B);
	signal weight_2 : sfixed(A downto -B) := to_sfixed(0.9, A, -B);
	
	signal input_1 : sfixed(A downto -B) := to_sfixed(2.3, A, -B);
	signal input_2 : sfixed(A downto -B) := to_sfixed(-1.5, A, -B);
	
begin
	--clock
    clk_tb <= not clk_tb after PERIOD;

    uut: neuron port map (clk_tb, res_tb, ena_tb, input_tb, weight_tb, output_tb);
    
--    next_value <= input_tb + 1;
    --next_value <= std_logic_vector( unsigned(input_tb) + 1 );

    stim_proc : process
    begin
        --for i in 0 to N_INPUTS loop
        --    wait for period;
        --   	input_tb(i) <=  
        --end loop;
        ena_tb <= '1';
        input_tb <= std_logic_vector(input_1);
        weight_tb <= std_logic_vector(weight_1);
        wait for PERIOD;
        input_tb <= std_logic_vector(input_2);
        weight_tb <= std_logic_vector(weight_2);
        wait for PERIOD;
        wait for PERIOD;
        wait for 10*PERIOD;
        res_tb <= '1';
        wait for 10*PERIOD;
        ena_tb <= '0';
        res_tb <= '0';
        wait for 10*PERIOD;
        
        assert (false) report "end" severity failure; -- to stop sim
    end process;
end behavioral;

