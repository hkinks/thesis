library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
library std;
use std.textio.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;

entity top_module_tb is
end top_module_tb;

architecture testbench of top_module_tb is
	component top_module is
		port (
        clk : in std_logic;
        push_button : in std_logic_vector(1 downto 0)
        -- push_button : in std_logic_vector(4 downto 0);
        -- inputA : in std_logic_vector(7 downto 0);
        -- inputB : in std_logic_vector(7 downto 0);
        -- output_ss : out std_logic_vector(7 downto 0); -- seven segment display
        -- output_led : out std_logic_vector(15 downto 0);
        -- sel : out std_logic_vector(7 downto 0)
		);
	end component;

	-- local signals
  signal clk : std_logic := '0';
  constant period : time := 5 ns;

	signal push_button : std_logic_vector(1 downto 0);
    signal debug : std_logic_vector(MEM_W_N-1 downto 0);
	-- signal push_button : std_logic_vector(4 downto 0);
	-- signal inputA : std_logic_vector(7 downto 0);
	-- signal inputB : std_logic_vector(7 downto 0);
	-- signal output_ss : std_logic_vector(7 downto 0); -- seven segment display
	-- signal output_led : std_logic_vector(15 downto 0);
	-- signal sel : std_logic_vector(7 downto 0);

	begin
		uut: top_module port map (clk, push_button);
		-- uut: top_module port map (clk, push_button, inputA, inputB, output_ss, output_led, sel);
	    clk <= not clk after 1538 ps;
		stim_proc : process
		begin
			wait for period;
			push_button(0) <= '1';
			wait for period;
			push_button(0) <= '0';
			wait for 1000 us;
            push_button(1) <= '1';
            wait for period;
            push_button(1) <= '0';
            wait for 1000 us;
            push_button(0) <= '1';
            wait for period;
            push_button(0) <= '0';
            wait for 1000 us;
			assert (false) report "end" severity failure; -- to stop sim
		end process;
end testbench;
