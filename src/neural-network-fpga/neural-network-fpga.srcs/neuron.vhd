library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity neuron is
    port(
    	clk : in std_logic;
    	res : in std_logic;
    	ena : in std_logic;
        act : in std_logic;
    	x : in std_logic_vector(BIT_WIDTH-1 downto 0);
    	w : in std_logic_vector(BIT_WIDTH-1 downto 0);
        y : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end neuron;

architecture behavioral of neuron is
signal sum : signed(BIT_WIDTH*2-1 downto 0) := (others => '0');
signal sum_in : signed(BIT_WIDTH*2-1 downto 0) := (others => '0');
signal trunc_sum : signed (BIT_WIDTH-1 downto 0) := (others => '0');
signal overflow : std_logic := '0';

component logsig is
  port (
    clk : in std_logic;
    enable : in std_logic;
    input : in std_logic_vector(BIT_WIDTH-1 downto 0);
    output : out std_logic_vector(BIT_WIDTH-1 downto 0)
  );
end component;

begin
	activation_function: logsig port map(clk, act, std_logic_vector(trunc_sum), y);
    sum_in <= sum;
    truncation : process (clk) is
    begin
        -- detect overflow when truncating
        if sum >= signed(to_sfixed(MEM_I_N-1, sum'length/2-1, -sum'length/2)) then
            overflow <= '1';
            trunc_sum <= signed(to_sfixed(MEM_I_N-1, trunc_sum'length/2-1, -trunc_sum'length/2));
        elsif sum < signed(to_sfixed(-MEM_I_N, sum'length/2-1, -sum'length/2)) then
            overflow <= '1';
            trunc_sum <= signed(to_sfixed(-MEM_I_N, trunc_sum'length/2-1, -trunc_sum'length/2));
        else
            trunc_sum <= sum(3*BIT_WIDTH/2-1 downto BIT_WIDTH/2);
            overflow <= '0';
        end if;
    end process;

	add_and_multiply: process (clk) is
	begin
		if (rising_edge(clk)) then
			if (res = '1') then
				sum <= (others => '0');
			elsif (ena = '1') then
				sum <= sum_in + (signed(x) * signed(w));
			end if;
		end if;
	end process;

end behavioral;
