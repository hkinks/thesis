library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity seven_segment is
  port (
    clk : in std_logic;
    input : in std_logic_vector(3 downto 0);
    output : out std_logic_vector(6 downto 0)
  );
end seven_segment;

architecture behavioral of seven_segment is
begin
    process (input, clk) is
    begin
        if(rising_edge(clk)) then
            case input is
                when "0000" => output <= "1111110";
                when "0001" => output <= "0110000";
                when "0010" => output <= "1101101";
                when "0011" => output <= "1111001";
                when "0100" => output <= "0110011";
                when "0101" => output <= "1011011";
                when "0110" => output <= "1011111";
                when "0111" => output <= "1110000";
                when "1000" => output <= "1111111";
                when "1001" => output <= "1111011";
                when "1010" => output <= "1110111";
                when "1011" => output <= "0011111";
                when "1100" => output <= "1001110";
                when "1101" => output <= "0111101";
                when "1110" => output <= "1001111";
                when "1111" => output <= "0000000";
                when others => output <= "XXXXXXX";
            end case;
        end if;
    end process;
end behavioral;
