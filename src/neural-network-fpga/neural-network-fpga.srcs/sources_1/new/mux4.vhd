library ieee;
use ieee.std_logic_1164.all;

entity mux4 is
	generic (
		n : integer
	);
    port (
  		sel : in std_logic_vector(1 downto 0);
  		x0 : in std_logic_vector(n-1 downto 0);
  		x1 : in std_logic_vector(n-1 downto 0);
  		x2 : in std_logic_vector(n-1 downto 0);
  		x3 : in std_logic_vector(n-1 downto 0);
  		y : out std_logic_vector(n-1 downto 0)
    );
end mux4;

architecture behavioral of mux4 is

begin
    with sel select
        y <= x0 when "00",
             x1 when "01",
             x2 when "10",
             x3 when "11",
             (others => '0') when others;
end behavioral;
