library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;

entity display is
    port (
     --    inputB : in std_logic_vector(7 downto 0);        -- output_led : out std_logic_vector(15 downto 0);
        clk : in std_logic;
        inputA : in std_logic_vector(3 downto 0);
        output_ss : out std_logic_vector(6 downto 0); -- seven segment display
        sel : out std_logic_vector(7 downto 0)
    );
end display;

architecture behavioral of display is

    -- constants
    constant DIV : integer := 50000; -- clock divider for 1 ms delay
    constant DIV_1S : integer := 50000000; -- clock divider for 1 s delay

    -- 7 segment display
    signal sel_buf : std_logic_vector(7 downto 0) := "11111110";
    signal output_ss_buf : std_logic_vector(6 downto 0);
    signal input_buf : std_logic_vector(3 downto 0);
    signal input_bcd : std_logic_vector(7 downto 0);
    -- signal output_led_buf : std_logic_vector(15 downto 0) := (others => '0');

component seven_segment is
    port (
        clk : in std_logic;
        input : in std_logic_vector(3 downto 0);
        output : out std_logic_vector(6 downto 0)
    );
end component;

begin
    seven_seg: seven_segment port map (
        clk => clk,
        input => input_buf,
        output => output_ss_buf
    );

    choose_number : process(sel_buf) is
    begin
        input_bcd <= to_bcd(inputA);
        case sel_buf is
            when "11111110" =>
                input_buf <= input_bcd(3 downto 0);
            when "11111101" =>
                input_buf <= input_bcd(7 downto 4);
            when "11101111" =>
                if (inputA(0) = '1') then
                    input_buf <= "0001";
                else
                    input_buf <= "0000";
                end if;
            when "11011111" =>
                if (inputA(1) = '1') then
                    input_buf <= "0001";
                else
                    input_buf <= "0000";
                end if;
            when "10111111" =>
                if (inputA(2) = '1') then
                    input_buf <= "0001";
                else
                    input_buf <= "0000";
                end if;
            when "01111111" =>
                if (inputA(3) = '1') then
                    input_buf <= "0001";
                else
                    input_buf <= "0000";
                end if;

            when others =>
                input_buf <= "1111";
        end case;
    end process;

    segment_rotate : process(clk) is
    variable i : integer range 0 to DIV;
    begin
        if (rising_edge(clk)) then
            if (i = DIV) then
                i := 0;
                sel_buf <= sel_buf(0) & sel_buf(7 downto 1); -- rotate rightd
            else
                i := i+1;
            end if;
        end if;
    end process;

    sel <= sel_buf;
    output_ss <= not output_ss_buf;

end behavioral;
