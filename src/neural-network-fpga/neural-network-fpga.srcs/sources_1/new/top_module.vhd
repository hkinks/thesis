library ieee;
use ieee.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conf.all;

entity top_module is
	port (
        clk : in std_logic;
        push_button : in std_logic_vector(1 downto 0);
--        push_button : in std_logic_vector(4 downto 0);
--        inputA : in std_logic_vector(7 downto 0);
--        inputB : in std_logic_vector(7 downto 0);
       output_ss : out std_logic_vector(6 downto 0); -- dot is not being used
    --    output_led : out std_logic_vector(15 downto 0)
       sel : out std_logic_vector(7 downto 0)
    );
end top_module;

architecture behavioral of top_module is

component controller is
	port (
        clk : in std_logic;
        start : in std_logic;
        restart : in std_logic;
        weight_in : in std_logic_vector(BIT_WIDTH-1 downto 0);
        read_ena : out std_logic;
        wb_ena : out std_logic;
        mux_i_sel : out std_logic_vector;
        reset_sum : out std_logic;
        max_ena : out std_logic;
        calculate : out std_logic;
        activate : out std_logic;
        reinit : out std_logic;
        neuron_index : out std_logic_vector(3 downto 0);
        mem_i_adr : out std_logic_vector(MEM_I_N-1 downto 0);
        mem_w_adr : out std_logic_vector(MEM_W_N-1 downto 0);
        mem_r_adr : out std_logic_vector(MEM_R_N-1 downto 0)
	);
end component;

component blk_mem_res
    port (
        clka : in std_logic;
        ena : in std_logic;
        wea : in std_logic_vector(0 downto 0);
        addra : in std_logic_vector(MEM_R_N-1 downto 0);
        dina : in std_logic_vector(7 downto 0);
        douta : out std_logic_vector(7 downto 0)
    );
end component;

component blk_mem_img
    port (
        clka : in std_logic;
        ena : in std_logic;
        wea : in std_logic_vector(0 downto 0);
        addra : in std_logic_vector(MEM_I_N-1 downto 0);
        dina : in std_logic_vector(7 downto 0);
        douta : out std_logic_vector(7 downto 0)
    );
end component;

component blk_mem_gen_0
    port (
        clka : in std_logic;
        ena : in std_logic;
        wea : in std_logic_vector(0 downto 0);
        addra : in std_logic_vector(MEM_W_N-1 downto 0);
        dina : in std_logic_vector(7 downto 0);
        douta : out std_logic_vector(7 downto 0)
    );
end component;

component neuron
    port (
        clk : in std_logic;
        res : in std_logic;
        ena : in std_logic;
        act : in std_logic;
        x : in std_logic_vector(BIT_WIDTH-1 downto 0);
        w : in std_logic_vector(BIT_WIDTH-1 downto 0);
        y : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end component;

component reg
    port(
    	clk : in std_logic;
    	res : in std_logic;
    	ena : in std_logic;
    	x : in std_logic_vector(BIT_WIDTH-1 downto 0);
        y : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end component;

component mux4
	generic (
		N : integer
	);
	port (
  		sel : in std_logic_vector(1 downto 0);
  		x0 : in std_logic_vector(n-1 downto 0);
  		x1 : in std_logic_vector(n-1 downto 0);
  		x2 : in std_logic_vector(n-1 downto 0);
  		x3 : in std_logic_vector(n-1 downto 0);
  		y : out std_logic_vector(n-1 downto 0)
	);
end component;

component max is
    port (
		clk : in std_logic;
        ena : in std_logic;
        res : in std_logic;
        x : in std_logic_vector(BIT_WIDTH-1 downto 0);
        index : in std_logic_vector(3 downto 0);
        y : out std_logic_vector(BIT_WIDTH-1 downto 0);
        max_index : out std_logic_vector(3 downto 0)
    );
end component;

component display is
    port (
        clk : in std_logic;
        inputa : in std_logic_vector(3 downto 0);
        output_ss : out std_logic_vector(6 downto 0); -- seven segment display
--        output_led : out std_logic_vector(15 downto 0);
        sel : out std_logic_vector(7 downto 0)
    );
end component;

------------- wires ----------------------
-- controller
signal read_ena: std_logic := '0';
signal mux_i_sel : std_logic_vector(1 downto 0) := "11";
signal calculate : std_logic := '0';
signal activate : std_logic := '0';

-- neuron
signal neuron_res : std_logic;
signal neuron_ena : std_logic;
signal neuron_y : std_logic_vector(BIT_WIDTH-1 downto 0);

-- Block rams
-- weight BRAM
signal bram_w_adr : std_logic_vector(MEM_W_N-1 downto 0);
signal bram_w_in : std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
signal bram_w_wea : std_logic_vector(0 to 0) := (others => '0');
signal bram_w_out : std_logic_vector(BIT_WIDTH-1 downto 0);
-- image BRAM
signal bram_i_ena : std_logic := '0';
signal bram_i_adr : std_logic_vector(MEM_I_N-1 downto 0);
signal bram_i_in : std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
signal bram_i_wea : std_logic_vector(0 to 0) := (others => '0');
signal bram_i_out : std_logic_vector(BIT_WIDTH-1 downto 0);
-- result BRAM
signal bram_r_ena : std_logic := '0';
signal bram_r_adr : std_logic_vector(MEM_R_N-1 downto 0);
-- signal bram_r_in : std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
signal bram_r_wea : std_logic_vector(0 to 0) := (others => '0');
signal bram_r_out : std_logic_vector(BIT_WIDTH-1 downto 0);

-- mux -> neuron
signal mux_out : std_logic_vector(BIT_WIDTH-1 downto 0);
signal bias : std_logic_vector(BIT_WIDTH-1 downto 0) := (FRACTION => '1', others => '0');
signal zero : std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');

-- max finder
signal max_ena : std_logic;
signal max_res : std_logic;

-- display
signal max_output : std_logic_vector(BIT_WIDTH-1 downto 0) ;
signal neuron_index : std_logic_vector(3 downto 0);
signal max_index : std_logic_vector(3 downto 0) := (others => '0');

begin
	ctrl : controller port map (clk, push_button(0), push_button(1), bram_w_out, read_ena, bram_r_wea(0),
        mux_i_sel, neuron_res, max_ena, calculate, activate, max_res, neuron_index,
        bram_i_adr, bram_w_adr, bram_r_adr);
    neuron_0 : neuron port map (clk, neuron_res, calculate, activate, mux_out, bram_w_out, neuron_y);
    ram_i : blk_mem_img port map (clk, read_ena, bram_i_wea, bram_i_adr, bram_i_in, bram_i_out);
    ram_w : blk_mem_gen_0 port map (clk, read_ena, bram_w_wea, bram_w_adr, bram_w_in, bram_w_out);
    ram_r : blk_mem_res port map (clk, read_ena, bram_r_wea, bram_r_adr, neuron_y, bram_r_out);
    mux_i : mux4 generic map (BIT_WIDTH) port map (mux_i_sel, bram_i_out, bram_r_out, bias, zero, mux_out);
    max_0 : max port map (clk, max_ena, max_res, neuron_y, neuron_index, max_output, max_index);
    display_0 : display port map (clk, max_index, output_ss, sel);

end behavioral;
