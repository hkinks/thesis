# 
# Synthesis run script generated by Vivado
# 

set_param xicom.use_bs_reader 1
set_msg_config -id {Common 17-41} -limit 10000000
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
set_msg_config  -ruleid {1}  -severity {INFO}  -suppress 
create_project -in_memory -part xc7a100tcsg324-1

set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.cache/wt [current_project]
set_property parent.project_path E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property vhdl_version vhdl_2k [current_fileset]
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/weights.coe
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/image4.coe
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/image20.coe
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/image14.coe
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/weights_pruned.coe
add_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/coef/image5.coe
add_files -quiet E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_gen_0_synth_1/blk_mem_gen_0.dcp
set_property used_in_implementation false [get_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_gen_0_synth_1/blk_mem_gen_0.dcp]
add_files -quiet E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_img_synth_1/blk_mem_img.dcp
set_property used_in_implementation false [get_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_img_synth_1/blk_mem_img.dcp]
add_files -quiet E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_res_synth_1/blk_mem_res.dcp
set_property used_in_implementation false [get_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.runs/blk_mem_res_synth_1/blk_mem_res.dcp]
read_vhdl -library xil_defaultlib {
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/conf.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/neuron.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/controller.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/seven_segment.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/logsig.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/sources_1/new/display.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/sources_1/new/top_module.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/sources_1/new/mux4.vhd
  E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/sources_1/new/max.vhd
}
read_xdc E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/constrs_1/new/constraints.xdc
set_property used_in_implementation false [get_files E:/Workspace/thesis3/src/neural-network-fpga/neural-network-fpga.srcs/constrs_1/new/constraints.xdc]

synth_design -top top_module -part xc7a100tcsg324-1
write_checkpoint -noxdef top_module.dcp
catch { report_utilization -file top_module_utilization_synth.rpt -pb top_module_utilization_synth.pb }
