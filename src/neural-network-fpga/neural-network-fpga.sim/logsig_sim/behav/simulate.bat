@echo off
set xv_path=C:\\Xilinx\\Vivado\\2015.4\\bin
call %xv_path%/xsim logsig_tb_behav -key {Behavioral:logsig_sim:Functional:logsig_tb} -tclbatch logsig_tb.tcl -view C:/Workspace/master-thesis/src/neural-network-fpga/neural-network-fpga.sim/logsig_tb_behav.wcfg -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
