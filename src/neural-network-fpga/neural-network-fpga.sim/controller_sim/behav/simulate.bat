@echo off
set xv_path=C:\\Xilinx\\Vivado\\2015.4\\bin
call %xv_path%/xsim controller_tb_behav -key {Behavioral:controller_sim:Functional:controller_tb} -tclbatch controller_tb.tcl -view C:/Workspace/master-thesis/src/neural-network-fpga/neural-network-fpga.sim/controller_tb_behav.wcfg -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
