# README #
Source files for master thesis _Implementing Neural Networks on Field Programmable Gate Array_ by Hannes Kinks.

1. It includes the thesis itself under directory 'latex' (thesis.pdf).
2. The code related to the thesis is under 'src', where

 * matlab - experiments carried out in matlab

 * python - early tests with PyBrain

 * neural-network-fpga - vhdl code for NN synthesis on FPGA

