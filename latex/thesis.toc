\contentsline {section}{\numberline {1}Introduction}{10}{section.1}
\contentsline {subsection}{\numberline {1.1}Background and motivation}{10}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Problem and goal definition}{11}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Methodology}{12}{subsection.1.3}
\contentsline {section}{\numberline {2}Theory}{14}{section.2}
\contentsline {subsection}{\numberline {2.1}Artificial neural networks}{14}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Artificial neuron}{14}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Feedforward neural network}{19}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Constructive Learning}{20}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Evolutionary computation}{20}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Genetic algorithms}{20}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Field Programmable Gate Array basics}{22}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}FPGA implementation of neural networks}{23}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Data representation and precision}{26}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Activation function implementation}{26}{subsection.2.6}
\contentsline {section}{\numberline {3}Face recognition task}{28}{section.3}
\contentsline {subsection}{\numberline {3.1}Neural network models}{28}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Experiments with optimizing the network}{29}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Rounding the weights}{30}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Pruning insignificant connections}{31}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Exploring layer connections with genetic algorithm}{32}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Optimization of individual connections with GA}{35}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}Approximation of activation function}{37}{subsection.3.3}
\contentsline {section}{\numberline {4}Hardware realization}{39}{section.4}
\contentsline {subsection}{\numberline {4.1}Design process}{39}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Design choices}{40}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Architecture}{41}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Simulation and verification}{44}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Xilinx Artix-7 FPGA overview}{45}{subsubsection.4.4.1}
\contentsline {subsection}{\numberline {4.5}Results}{46}{subsection.4.5}
\contentsline {section}{\numberline {5}Conclusion}{49}{section.5}
